FROM php:8.1-apache

RUN apt-get update && apt-get install -y \
git \
unzip \
curl

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

WORKDIR /var/www/html

ADD . .

RUN composer install

#RUN composer dump-autoload

RUN php artisan key:generate

EXPOSE 80
